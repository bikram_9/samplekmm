package com.example.myapplicationkmm

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform